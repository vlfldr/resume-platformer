A tile-based platformer and level editor. Monospace text tiles allow for building interactive documents. 

Tileset: https://trixelized.itch.io/starstring-fields

Player sprite: https://chierit.itch.io/elementals-wind-hashashin
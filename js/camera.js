import * as Consts from './constants.js';

export default class Camera {
    constructor() {
        this.x = 0;
        this.y = 0;
    }


    update(player) {
        let lastCam = structuredClone(this);
        this.x = player.position.x - (Consts.TILE_SIZE * (Consts.VIEWPORT_TILES_W / 2))
        this.y = player.position.y - (Consts.TILE_SIZE * (Consts.VIEWPORT_TILES_H / 2))

        // canvas bounds
        if (this.x < 0)     this.x = 0;
        if (this.x > (Consts.TILE_SIZE * 50))   this.x = Consts.TILE_SIZE * 50;
        if (this.y < 0)     this.y = 0;
        if (this.y > (Consts.TILE_SIZE * 31))   this.y = Consts.TILE_SIZE * 31;

        // smooth camera movement (causes blurry text rendering)
        this.x = lastCam.x - (lastCam.x - this.x) * .1;
        this.y = lastCam.y - (lastCam.y - this.y) * .1;

        // use ints for smooth text rendering (causes camera jitter)
        this.x = Math.round(this.x)
        this.y = Math.round(this.y)
    }
}
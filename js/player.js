import * as Consts from "./constants.js";

let playerSpritesheet = new Image();
let playerSpritesheetFlipped = new Image();
playerSpritesheet.src = 'res/img/player.png';
playerSpritesheetFlipped.src = 'res/img/player_flipped.png';

export default class Player {
    constructor() {
        this.position = {
            x: Consts.TILE_SIZE * 4,
            y: Consts.TILE_SIZE * 3
        };
        this.velocity = {
            x: 0.0,
            y: Infinity
        };
        this.size = {
            x: 6,
            y: 10,
        };
        this.moveSpeed = .23;
        this.framesSinceGrounded = 0;
        this.framesOnWall = 0;
        this.animState = 'idle';
        this.animFrame = 0;
        this.flipX = false;
        this.onWall = 0;    // 0 for false, 1 for right, -1 for left
    }

    draw(ctx) {
        let drawPos = { 
            x: Math.round(this.position.x), 
            y: Math.round(this.position.y) 
        };
    
        let drawSheet = (this.flipX) ? playerSpritesheetFlipped : playerSpritesheet;
        let drawFrame = this.animFrame * Consts.SS_PLAYER_W;
        if(this.flipX)    drawFrame = playerSpritesheet.naturalWidth - drawFrame - Consts.SS_PLAYER_W;
    
        ctx.drawImage(drawSheet, drawFrame, Consts.ANIMATIONS[this.animState]['row'] * Consts.SS_PLAYER_H, 
            Consts.SS_PLAYER_W, Consts.SS_PLAYER_H,
            drawPos.x + this.size.x - Consts.SS_X_OFFSET/3, 
            drawPos.y + this.size.y/3 - Consts.SS_Y_OFFSET/3,
            Consts.SS_PLAYER_W/3, Consts.SS_PLAYER_H/3 );
    
        //ctx.strokeRect(this.position.x, this.position.y, this.size.x, this.size.y);    // debug hitbox
    }

    /* ref: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
        0. Decompose X/Y movement; step X first
    *   1. Get coordinate of leading edge based on velocity
    *   2. Get tiles on OPPOSITE axis intersected by leading edge
    *       IMPORTANT:  Because our max speed is below Consts.TILE_SIZE, we do not have to search more than one tile.
    *           If player can move more than Consts.TILE_SIZE in one frame, additional tiles must be checked.
    *   3. Check collisions with leading edge
    *   4. Snap to tile borders if next position is invalid
    *   5. Advance along X axis; recalc leading edge position
    *   6. Step Y using same process
    */

    update(tileMap) {
        // max speed
        if(Math.abs(this.velocity.x) > Consts.MAX_X_SPEED)   
        this.velocity.x = Consts.MAX_X_SPEED * Math.sign(this.velocity.x);

        // damping
        if(this.velocity.x != 0)  this.velocity.x = this.velocity.x / (1 + .05)

        let nextX = this.position.x + this.velocity.x;
        let xEdge = nextX;
        if(this.velocity.x > 0)   xEdge += this.size.x;

        // horizontal page bounds
        if(xEdge < 0) {
            nextX = 0;
            this.velocity.x = 0;
        }
        if(xEdge > (Consts.TILE_SIZE * (tileMap[0].length-1))){
            nextX = (Consts.TILE_SIZE * (tileMap[0].length-1)) - this.size.x
            this.velocity.x = 0;
        }

        // horizontal object collision
        if(this.xCollision(xEdge, this.position.y, tileMap)) {
            if(this.velocity.x > 0)   nextX = (( Math.floor(xEdge / Consts.TILE_SIZE)) * Consts.TILE_SIZE) - this.size.x;
            else                      nextX = (( Math.floor(xEdge / Consts.TILE_SIZE) + 1) * Consts.TILE_SIZE);

            // begin wall grab
            if(this.velocity.y != Infinity) {
                // prevent unintentional just-walked-off/just-jumped wall grabs
                if(this.onWall == 0 && this.framesSinceGrounded < 15) {
                    this.onWall = 0;

                // onWall stores directional information for wall jump
                } else {
                    this.onWall = Math.sign(this.velocity.x);
                    this.velocity.y = 0;
                }
            }
            this.velocity.x = 0;
        }
        else    this.onWall = 0;

        this.position.x = nextX;

        // on solid ground
        if(this.velocity.y == Infinity) {
            let nextY = this.position.y + Consts.GRAVITY;
            this.framesSinceGrounded = 0;
            
            if(!this.yCollision(this.position.x, nextY, tileMap))  this.velocity.y = 0;
        }

        // falling/jumping
        if(this.velocity.y != Infinity) {
            this.framesSinceGrounded++;

            // only add gravity if not wall grabbing
            if(this.onWall != 0) {
                this.framesOnWall++;
                this.framesSinceGrounded = 0;

                // slide down wall after 50 frames
                if(this.framesOnWall > 50) {
                    this.velocity.y += Consts.GRAVITY * (this.framesOnWall * 0.02)
                }
            }
            else {
                this.velocity.y += Consts.GRAVITY;
                this.framesOnWall = 0;
            }

            // terminal velocity
            if(this.velocity.y > Consts.MAX_Y_SPEED)     this.velocity.y = Consts.MAX_Y_SPEED;

            let nextY = this.position.y + this.velocity.y;
            let yEdge = nextY;
            if(this.velocity.y > 0)   yEdge += this.size.y;

            // vertical page bounds
            if(yEdge < 0) {
                nextY = 0;
                this.velocity.y = 0;
            }
            if(yEdge > (Consts.TILE_SIZE * (tileMap.length-1))) {
                nextY = (Consts.TILE_SIZE * (tileMap.length-1)) + this.size.y;
                this.velocity.y = 0;
            }

            // vertical object collision
            if(this.yCollision(this.position.x, yEdge, tileMap)) {
                // if hit ceiling, snap to tile below & bump out 1 pixel
                if(this.velocity.y <= 0 ) {
                    nextY = ((Math.floor(yEdge / Consts.TILE_SIZE) + 1) * Consts.TILE_SIZE) + 1
                    this.velocity.y = 0;
                }
                // if hit floor, snap above & bump up
                else {
                    nextY = ((Math.floor(yEdge / Consts.TILE_SIZE) - 1) * Consts.TILE_SIZE) - 1   
                    this.velocity.y = Infinity;
                }
            }
            this.position.y = nextY;
        }   
    }

    animate() {
        this.animState = 'idle'
        if(Math.abs(this.velocity.x) > 1)                           this.animState = 'run'
        if(this.velocity.y != Infinity && this.velocity.y >= 0)     this.animState = 'fall';
        if(this.velocity.y < 0)                                     this.animState = 'jump';
        if(this.onWall != 0)                                        this.animState = 'grab';

        // cycle frames
        if(this.animFrame >= Consts.ANIMATIONS[this.animState]['frames'])  this.animFrame = 0;
    }

    // returns TRUE if COLLISION
    xCollision(x, y, tileMap) {
        let yTopTile = this.worldToTile(x, y, tileMap);
        // always search bottom tile as player is 2 tiles high
        let yBotTile = this.worldToTile(x, y + Consts.TILE_SIZE - 1, tileMap) 

        // one-way platforms are never an obstacle on X axis
        return (yTopTile && yTopTile.tileSolid && !yTopTile.tileOneWay)
            || (yBotTile && yBotTile.tileSolid && !yBotTile.tileOneWay)
    }

    // returns TRUE if COLLISION
    yCollision(x, y, tileMap) {
        let xLeftTile = this.worldToTile(x, y, tileMap)
        // xRightTile != xLeftTile ONLY IF player is straddling two tiles
        //      otherwise, it will be the same as xLeftTile
        let xRightTile = this.worldToTile(x + this.size.x - 1, y, tileMap)
        
        let hit = (xRightTile && xRightTile.tileSolid)
            || (xLeftTile && xLeftTile.tileSolid)

        // one-way platforms
        // ref: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
        let hitY = Math.floor(y / Consts.TILE_SIZE) * Consts.TILE_SIZE;
        if (hit && ((xRightTile && xRightTile.tileOneWay)
            || (xLeftTile && xLeftTile.tileOneWay))
        )       hit = (hitY - this.position.y > Consts.TILE_SIZE);

        return hit
    }

    // returns null if OOB of tilemap
    worldToTile(x, y, tileMap) { 
        let xTile = Math.floor(x / Consts.TILE_SIZE)
        let yTile = Math.floor(y / Consts.TILE_SIZE)
        if(yTile > tileMap.length || yTile < 0
            || xTile > tileMap[0].length || xTile < 0)  return null;

        return tileMap[yTile][xTile] 
    }
}
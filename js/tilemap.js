import * as Consts from './constants.js';
import Tile from './tile.js';

let tileSheet = new Image();
let backgroundImg = new Image();
let fontSpritesheet = new Image();

tileSheet.src = 'res/img/tilesheet.png';
backgroundImg.src = 'res/img/bg.png';
fontSpritesheet.src = 'res/fonts/fira.png';

export default class TileMap {
    constructor() {
        this.tiles = null;
    }

    // invokes tilemapFromString() to build 2D array from file upload
    importMap() {
        let input = document.createElement('input');
        input.type = 'file';
        input.val = null;
        input.onchange = e => { 
            let reader = new FileReader();
            reader.readAsText(e.target.files[0],'UTF-8');
            reader.onload = readerEvent => { 
                this.tiles = this.tilemapFromString(readerEvent.target.result)
            }
        }
        input.click();
    }

    // export to localStorage
    exportToBrowser() {
        window.localStorage.setItem("tileMap", JSON.stringify(this.tiles));
        alert('Map saved to localStorage!');
    }

    // export to map.txt & download
    exportToFile() {
        let str = ''
        this.tiles.forEach(row => {
            row.forEach(tile => {
                str += tile.tileCode + ' ';
            });
            str += '\n';
        });

        let element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(str));
        element.setAttribute('download', 'map.txt');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    // 2D array from string
    tilemapFromString(str) {
        let rowStrings = str.split('\n');
        let rows = [];

        for (let i = 0; i < rowStrings.length; i++) {
            let tmpRow = [];
            for (let j = 0; j < rowStrings[i].length; j+=3) {
                tmpRow.push(new Tile(rowStrings[i].substring(j, j+2)))
            }
            rows.push(tmpRow);
        }

        return rows;
    }

    drawSprite(tileX, tileY, sprite, ctx){
        ctx.drawImage(tileSheet, sprite.x * Consts.SS_TILE_SIZE, sprite.y * Consts.SS_TILE_SIZE, 
            Consts.SS_TILE_SIZE * sprite.w, Consts.SS_TILE_SIZE * sprite.h, tileX * Consts.TILE_SIZE, tileY * Consts.TILE_SIZE, 
            Consts.TILE_SIZE * sprite.w, Consts.TILE_SIZE * sprite.h)
    }

    drawLetter(tileX, tileY, letter, ctx){
        let charCode = letter.charCodeAt(0) - 32;   // temp hack
        
        ctx.drawImage(fontSpritesheet, 43 * charCode, 0, 43, 64,
            (tileX * Consts.TILE_SIZE) + 2, (tileY * Consts.TILE_SIZE) - 2.5, (Consts.TILE_SIZE-2)*1.4, Consts.TILE_SIZE*1.4)
    }

    draw(ctx, textCtx, bgCtx, camera, editMode) {
        // clear entire canvas
        if(editMode) {
            ctx.clearRect(0, 0, ctx.canvas.height*2, ctx.canvas.width*2);
            textCtx.clearRect(0, 0, ctx.canvas.height*2, ctx.canvas.width*2);

            textCtx.setTransform(2, 0, 0, 2, 0, 0);
            ctx.setTransform(1, 0, 0, 1, 0, 0);
        }
        // clear on-camera + a bit of overhang
        else {
            ctx.clearRect(camera.x-10, camera.y-10,
                (Consts.VIEWPORT_TILES_W * Consts.TILE_SIZE) + 10, 
                (Consts.VIEWPORT_TILES_H * Consts.TILE_SIZE) + 10);
            textCtx.clearRect(camera.x-10, camera.y-10,
                (Consts.VIEWPORT_TILES_W * Consts.TILE_SIZE) + 10, 
                (Consts.VIEWPORT_TILES_H * Consts.TILE_SIZE) + 10);

            // scale to viewport size & scroll
            ctx.setTransform(2, 0, 0, 2, -camera.x*2, -camera.y*2);
            textCtx.setTransform(4, 0, 0, 4, -camera.x*4, -camera.y*4);
        }

        // background w/ lazy parallax
        bgCtx.drawImage(backgroundImg, 
            Math.round(-camera.x * .5), 
            Math.round(-camera.y * .5)
        );

        // don't draw tiles outside the viewport
        let viewportY = (camera.y / Consts.TILE_SIZE);
        let viewportX = (camera.x / Consts.TILE_SIZE);
        let overdraw = 6;   // some sprites are up to 6 tiles wide/high

        if(editMode)    overdraw = 100;

        // draw tiles within viewport + overdraw
        this.tiles.forEach((row, i) => {
            if(i >= viewportY - overdraw 
            && i <= viewportY + Consts.VIEWPORT_TILES_H + overdraw){
                row.forEach((tile, j) => {
                    if(j >= viewportX - overdraw 
                    && j <= viewportX + Consts.VIEWPORT_TILES_W + overdraw){
                        if(tile.tileLetter != '')   this.drawLetter(j, i, tile.tileLetter, textCtx);
                        else                        this.drawSprite(j, i, tile.tileSprite, ctx);

                        // if(tile.tileDebug)          drawSprite(j, i, SPRITES[15]);
                        // tile.tileDebug = false;
                    }
                });
            }
        });
    }
}
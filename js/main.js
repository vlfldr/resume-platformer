import * as Consts from './constants.js';
import * as Utils from './utils.js';
import Player from './player.js';
import Camera from './camera.js';
import InputManager from './input.js';
import TileMap from './tilemap.js';
import Editor from './editor.js';

let ctx, textCtx, bgCtx;
let camera, input, player, tileMap, editor;

let onMobileDevice = /Mobile|Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
let gameFrame = 0;
let gameState = 'load';  // load, edit, play

function init() {
    let mapWidth = Consts.TILE_SIZE * 103;
    let mapHeight = Consts.TILE_SIZE * 64;

    ctx = Utils.createHiPPICanvas('main-canvas', mapWidth, mapHeight, 1)
        .getContext('2d', { imageSmoothingEnabled: false } );
    bgCtx = Utils.createHiPPICanvas('background-canvas', mapWidth/2, mapHeight/2, 1)
        .getContext('2d', { alpha: false, imageSmoothingEnabled: false } );
    // hi-res text layer. expensive!!
    textCtx = Utils.createHiPPICanvas('text-canvas', mapWidth*2, mapHeight*2, 1).getContext('2d');

    player = new Player();
    camera = new Camera();
    tileMap = new TileMap();
    input = new InputManager(onMobileDevice);

    // load map from local storage if previously saved
    if(window.localStorage.getItem('tileMap')) {     
        tileMap.tiles = JSON.parse(window.localStorage.getItem('tileMap'));
        gameState = 'play';
    }

    editor = new Editor(tileMap);

    // sidebar buttons
    document.getElementById('save-browser-btn')
        .addEventListener('click', tileMap.exportToBrowser.bind(tileMap), false);
    document.getElementById('save-file-btn')
        .addEventListener('click', tileMap.exportToFile.bind(tileMap), false);
    document.getElementById('load-btn')
        .addEventListener('click', tileMap.importMap.bind(tileMap), false);
    document.getElementById('editor-toggle-btn')
         .addEventListener('click', toggleEditor, false);

    window.requestAnimationFrame(mainLoop);
}

function toggleEditor() {
    if(gameState !== 'play')    gameState = 'play';
    else                        gameState = 'edit';

    editor.toggleEditor();
}

function mainLoop() {
    if(gameState === 'load') {
        if(tileMap.tiles) {
            editor.tileMap = tileMap.tiles;
            gameState = 'play';
        }
    }

    else if(gameState === 'edit') {
        tileMap.draw(ctx, textCtx, bgCtx, camera, true);
        editor.draw(ctx, textCtx, bgCtx);
    }

    else if(gameState === 'play') {        
        input.update(player);
        
        player.animate();
        player.update(tileMap.tiles);

        camera.update(player);

        tileMap.draw(ctx, textCtx, bgCtx, camera);
        player.draw(ctx);

        if(input.onMobileDevice) {
            input.drawMobileControls(ctx.canvas.getBoundingClientRect().height);
        }

        // animate sprites every 15 frames
        gameFrame++;
        if(gameFrame == 15) {
            gameFrame = 0;
            player.animFrame++;
        }
    }

    window.requestAnimationFrame(mainLoop);
}

// ENTRYPOINT
init();